package ru.t1.vlvov.tm.repository;

import ru.t1.vlvov.tm.api.repository.IProjectRepository;
import ru.t1.vlvov.tm.model.Project;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

}
