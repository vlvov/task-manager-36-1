package ru.t1.vlvov.tm.repository;

import ru.t1.vlvov.tm.api.repository.ISessionRepository;
import ru.t1.vlvov.tm.model.Session;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
