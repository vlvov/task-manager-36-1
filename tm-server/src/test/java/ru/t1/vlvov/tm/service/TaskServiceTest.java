package ru.t1.vlvov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.ISessionRepository;
import ru.t1.vlvov.tm.api.repository.ITaskRepository;
import ru.t1.vlvov.tm.api.service.ISessionService;
import ru.t1.vlvov.tm.api.service.ITaskService;
import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.marker.UnitCategory;
import ru.t1.vlvov.tm.model.Task;
import ru.t1.vlvov.tm.repository.SessionRepository;
import ru.t1.vlvov.tm.repository.TaskRepository;

import static ru.t1.vlvov.tm.constant.TaskTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @After
    public void tearDown() {
        taskService.clear();
    }

    @Test
    public void add() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, taskService.findAll().get(0));
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, taskService.findAll().get(0));
        Assert.assertEquals(USER1.getId(), taskService.findAll().get(0).getUserId());
    }

    @Test
    public void clearByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, taskService.findAll());
        taskService.clear(USER2.getId());
        Assert.assertFalse(taskService.findAll().isEmpty());
        taskService.clear(USER1.getId());
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER2_TASK1);
        taskService.clear(USER1.getId());
        Assert.assertFalse(taskService.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, taskService.findAll(USER1.getId()));
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1_TASK1);
        taskService.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1, taskService.findOneById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertNotEquals(USER2_TASK1, taskService.findOneById(USER1.getId(), USER2_TASK1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1_TASK1);
        taskService.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1, taskService.remove(USER1.getId(), USER1_TASK1));
        Assert.assertFalse(taskService.findAll().contains(USER1_TASK1));
        Assert.assertTrue(taskService.findAll().contains(USER2_TASK1));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1_TASK1);
        taskService.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1, taskService.removeById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertFalse(taskService.findAll().contains(USER1_TASK1));
        Assert.assertTrue(taskService.findAll().contains(USER2_TASK1));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        taskService.add(USER1_TASK1);
        Assert.assertTrue(taskService.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(taskService.existsById(USER2_TASK1.getId()));
    }

    @Test
    public void changeTaskStatusById() {
        taskService.add(USER1_TASK1);
        Assert.assertEquals(Status.NOT_STARTED, USER1_TASK1.getStatus());
        taskService.changeTaskStatusById(USER1_TASK1.getUserId(), USER1_TASK1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, USER1_TASK1.getStatus());
    }

    @Test
    public void createTaskName(){
        Assert.assertTrue(taskService.findAll().isEmpty());
        @NotNull Task task = taskService.create(USER1.getId(), "test_task");
        Assert.assertEquals(task, taskService.findOneById(task.getId()));
        Assert.assertEquals("test_task", task.getName());
        Assert.assertEquals(USER1.getId(), task.getUserId());
    }

    @Test
    public void createTaskNameDescription() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        @NotNull Task task = taskService.create(USER1.getId(), "test_task", "test_description");
        Assert.assertEquals(task, taskService.findOneById(task.getId()));
        Assert.assertEquals("test_task", task.getName());
        Assert.assertEquals("test_description", task.getDescription());
        Assert.assertEquals(USER1.getId(), task.getUserId());
    }

    @Test
    public void updateById() {
        Assert.assertTrue(taskService.findAll().isEmpty());
        @NotNull Task task = taskService.create(USER1.getId(), "test_task", "test_description");
        taskService.updateById(USER1.getId(), task.getId(), "new name", "new description");
        Assert.assertEquals("new name", task.getName());
        Assert.assertEquals("new description", task.getDescription());
    }

}
