package ru.t1.vlvov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.ISessionRepository;
import ru.t1.vlvov.tm.marker.UnitCategory;
import static ru.t1.vlvov.tm.constant.SessionTestData.*;
import static ru.t1.vlvov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @After
    public void tearDown() {
        sessionRepository.clear();
    }

    @Test
    public void add() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findAll().get(0));
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findAll().get(0));
        Assert.assertEquals(USER1.getId(), sessionRepository.findAll().get(0).getUserId());
    }

    @Test
    public void clearByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionRepository.findAll());
        sessionRepository.clear(USER2.getId());
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        sessionRepository.clear(USER1.getId());
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER2_SESSION1);
        sessionRepository.clear(USER1.getId());
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionRepository.findAll(USER1.getId()));
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findOneById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertNotEquals(USER2_SESSION1, sessionRepository.findOneById(USER1.getId(), USER2_SESSION1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.remove(USER1.getId(), USER1_SESSION1));
        Assert.assertFalse(sessionRepository.findAll().contains(USER1_SESSION1));
        Assert.assertTrue(sessionRepository.findAll().contains(USER2_SESSION1));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.removeById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertFalse(sessionRepository.findAll().contains(USER1_SESSION1));
        Assert.assertTrue(sessionRepository.findAll().contains(USER2_SESSION1));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        Assert.assertTrue(sessionRepository.existsById(USER1_SESSION1.getId()));
        Assert.assertFalse(sessionRepository.existsById(USER2_SESSION1.getId()));
    }

}
