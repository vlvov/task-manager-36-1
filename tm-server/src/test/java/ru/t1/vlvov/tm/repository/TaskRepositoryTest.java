package ru.t1.vlvov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.repository.ISessionRepository;
import ru.t1.vlvov.tm.api.repository.ITaskRepository;
import ru.t1.vlvov.tm.marker.UnitCategory;
import static ru.t1.vlvov.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.vlvov.tm.constant.UserTestData.*;
import static ru.t1.vlvov.tm.constant.TaskTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @After
    public void tearDown() {
        taskRepository.clear();
    }

    @Test
    public void add() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, taskRepository.findAll().get(0));
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, taskRepository.findAll().get(0));
        Assert.assertEquals(USER1.getId(), taskRepository.findAll().get(0).getUserId());
    }

    @Test
    public void clearByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, taskRepository.findAll());
        taskRepository.clear(USER2.getId());
        Assert.assertFalse(taskRepository.findAll().isEmpty());
        taskRepository.clear(USER1.getId());
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER2_TASK1);
        taskRepository.clear(USER1.getId());
        Assert.assertFalse(taskRepository.findAll().isEmpty());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, taskRepository.findAll(USER1.getId()));
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK1);
        taskRepository.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1, taskRepository.findOneById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertNotEquals(USER2_TASK1, taskRepository.findOneById(USER1.getId(), USER2_TASK1.getId()));
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK1);
        taskRepository.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1, taskRepository.remove(USER1.getId(), USER1_TASK1));
        Assert.assertFalse(taskRepository.findAll().contains(USER1_TASK1));
        Assert.assertTrue(taskRepository.findAll().contains(USER2_TASK1));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK1);
        taskRepository.add(USER2_TASK1);
        Assert.assertEquals(USER1_TASK1, taskRepository.removeById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertFalse(taskRepository.findAll().contains(USER1_TASK1));
        Assert.assertTrue(taskRepository.findAll().contains(USER2_TASK1));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK1);
        Assert.assertTrue(taskRepository.existsById(USER1_TASK1.getId()));
        Assert.assertFalse(taskRepository.existsById(USER2_TASK1.getId()));
    }

    @Test
    public void findAllByProjectIdByUserId() {
        Assert.assertTrue(taskRepository.findAll().isEmpty());
        taskRepository.add(USER1_TASK_LIST);
        taskRepository.add(USER2_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, taskRepository.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()));
    }

}
