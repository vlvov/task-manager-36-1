package ru.t1.vlvov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.experimental.categories.Category;
import ru.t1.vlvov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.vlvov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.vlvov.tm.api.endpoint.IUserEndpoint;
import ru.t1.vlvov.tm.api.service.IPropertyService;
import ru.t1.vlvov.tm.marker.IntegrationCategory;
import ru.t1.vlvov.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public class UserEndpointDomain {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);


}
